<?php

namespace App\Helpers;

use \PDF;
use Elibyy\TCPDF\TCPDF;

class PDFHelper extends TCPDF
{
    public static function sample()
    {
        $htmlContent = '<h1>Generate PDF using TCPDF</h1>';

        PDF::SetTitle('Sample PDF');
        PDF::AddPage();
        PDF::writeHTML($htmlContent, true, false, true, false, '');

        return PDF::Output('Sample.pdf');
    }

    public static function save()
    {
        $htmlContent = '<h1>Generate PDF using TCPDF</h1>';

        PDF::SetTitle('Sample PDF');
        PDF::AddPage();
        PDF::writeHTML($htmlContent, true, false, true, false, '');

        return PDF::Output( public_path('download/custom/path/' . uniqid() . '_Sample.pdf'), 'F' );
    }
}