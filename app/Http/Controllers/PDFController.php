<?php

namespace App\Http\Controllers;

use App\Helpers\PDFHelper;


class PDFController extends Controller
{
    public function sample() 
    {
        PDFHelper::sample();
    }

    public function save()
    {
        // create new PDF document
        $pdf = new PDFHelper('P', 'mm', 'A4', true, 'UTF-8', false);

        $htmlContent = '<h1>Generate PDF using TCPDF</h1>';

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 25);

        $pdf->setFooterCallback(function($pdf){
            // Position at 15 mm from bottom
            $pdf->SetY(-15);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(0, 10, 'Page ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        });

        $pdf->SetTitle('Sample PDF');
        $pdf->AddPage();
        $pdf->writeHTML($htmlContent, true, false, true, false, '');
        $pdf->Output( public_path('download/custom/path/' . uniqid() . '_Sample.pdf'), 'F' );
    }

    public function download()
    {
        $pdf = new PDFHelper('P', 'mm', 'A4', true, 'UTF-8', false);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 25);

        $pdf->setFooterCallback(function($pdf){
            // Position at 15 mm from bottom
            $pdf->SetY(-15);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(0, 10, 'Page ' . $pdf->getAliasNumPage() . '/' . $pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        });
        

        $pdf->SetTitle('Sample PDF');
        $pdf->AddPage();
        $pdf->ImageSVG(public_path('images/1296971.svg'), 120, 220, '', 50, '', '', '', 0, false);

        $pdf->AddPage();
        $pdf->Image(public_path('images/tree-576848_1280.png'), '', ( $pdf->getPageHeight() / $pdf->getScaleFactor() ), '', 100, 'PNG', '', 'C', false, 300, 'C');
        $pdf->Output( public_path('download/custom/path/' . uniqid() . '_Sample.pdf'), 'I' );
    }
}
